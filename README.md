# Tech Skills
## Avançado
 Markdown | 🟩🟩🟩🟩🟩
### Sistemas Operacionais
>Linux(Debian based) | 🟩🟩🟩🟩🟥

> Windows Server 

| Versão          | Proficiência|
|-----------------|--------------|
| 2012            | 🟩🟩🟩🟩🟥  |
| 2022 Standard   | 🟩🟩🟩🟥🟥  |
| 2022 Datacenter | 🟩🟥🟥🟥🟥  |

-  Docker | 🟩🟩🟩🟩🟥
-  Git | 🟩🟩🟩🟥🟥


## Básico
### CI/CD
| Ferramenta               | Proficiência|
|--------------------------|------------------|
| GitLab CI                | 🟩🟩🟥🟥🟥        |
| GitHub Actions (CI/CD)   | 🟩🟩🟥🟥🟥        |

## Programação
| Linguagem        | Proficiência|
|------------------|------------------|
| Python           | 🟩🟥🟥🟥🟥        |
| JavaScript       | 🟩🟥🟥🟥🟥        |
| Java             | 🟩🟥🟥🟥🟥        |
| HTML/PHP/CSS     | 🟩🟩🟥🟥🟥        |
| Bash/Shell       | 🟩🟩🟩🟥🟥        |

## Orquestração
| Ferramenta       | Proficiência|
|------------------|------------------|
| Terraform        | 🟩🟥🟥🟥🟥        |
| K8s              | 🟩🟥🟥🟥🟥        |
